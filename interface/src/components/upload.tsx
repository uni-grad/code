import React from "react"

const upload = () => {
  return (
    <svg
      className="w-12 h-12 p-2 text-gray-100 transition duration-150 ease-in-out bg-purple-600 rounded-full hover:bg-gray-100 hover:text-purple-600"
      fill="none"
      stroke="currentColor"
      viewBox="0 0 24 24"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
        d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12"
      />
    </svg>
  )
}

export default upload
