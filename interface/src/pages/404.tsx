import React from "react"

const _404 = () => {
  return (
    <div>
      <p>Page not found</p>
    </div>
  )
}

export default _404
